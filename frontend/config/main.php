<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'class' => 'rkit\components\Request',
            'web'=> '/frontend/web'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        
    ],
    'modules' => [
        'user' => [
            "id" => "user",
            'class' => 'common\modules\user\Module',
            'controllerMap' => [
                'default' => [
                    'class' => 'common\modules\user\controllers\DefaultController'   
                ],
                'franchise' => [
                    'class' => 'common\modules\user\controllers\FranchiseController'   
                ]
            ],
        ],
        'catalog' => [
            "id" => "catalog",
            'class' => 'common\modules\catalog\Module',
            'controllerMap' => [
                'category' => [
                    'class' => 'common\modules\catalog\controllers\CategoryController'   
                ]
            ],
        ],
        'file' => [
            "id" => "file",
            "class" => "common\modules\\file\Module",
            "controllerMap" => [
                "image" => [
                    "class" => 'common\modules\file\controllers\ImageController'
                ],
                "default" => [
                    "class" => 'common\modules\file\controllers\DefaultController'
                ]
            ]
        ]
    ],
    'params' => $params,
];

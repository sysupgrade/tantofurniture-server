<?php
namespace console\controllers;
use Yii;
use yii\console\Controller;

class FormController extends Controller {
    
    public $name;
    
    public $attrs;
    
    public $moduleName;
    
    
    public function options($id) {
        return ['name', 'attrs', "moduleName"];
    }
    
    public function optionAliases() {
        return ['n' => 'name', 'a' => 'attrs', "m" => "moduleName"];
    }
    
    public function actionCreate()
    {
        if(!$this->moduleName) {
            die("You need to specify the path");
        }
        $dirPath = "common/modules/" . $this->moduleName . "/forms/" . $this->name . ".php" ;
        $attributes = explode(",", $this->attrs);
        
        $text = $this->getHeaderText($this->name);
        $text .= $this->generateAttrs($attributes);
        $text .= $this->getFooterText();
        if (file_put_contents($dirPath, $text) !== false) {
        } else {
            echo "Cannot create file";
        }
    }
    
    private function generateAttrs($attrs) {
        $text = "    //attributes"
                . "\n";
        foreach($attrs as $attr) {
            $text .= "    public $" . $attr  . ";\n\n";
                    
        }
            
        return $text;
        
    }
    
    private function getHeaderText($name) {
        $moduleName = $this->moduleName;
        return 
"<?php
namespace common\modules\\$moduleName\\forms;

use rkit\components\RService;
/**
 * $name service
 *
 */
class $name extends RService
{
    
";
    }
    
    
    private function getFooterText() {
        return "

    public function rules() {
        return [
            
        ];
    }            
}";
    }
}
<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
        ],
        'migrate-user' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'migration_user',
            'migrationPath' => 'common\modules\user\migrations',
        ],
        'migrate-user-access' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'migration_user_access',
            'migrationPath' => 'common\modules\user\migrations-access',
        ],
        'migrate-catalog' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'migration_catalog',
            'migrationPath' => 'common\modules\catalog\migrations',
        ],
        'migrate-file' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'migration_file',
            'migrationPath' => 'common\modules\file\migrations',
        ]
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];

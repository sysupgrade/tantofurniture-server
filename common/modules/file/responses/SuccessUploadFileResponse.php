<?php
namespace common\modules\file\responses;

use rkit\components\RResponse;
/**
 * SuccessUploadFileResponse response
 *
 */
class SuccessUploadFileResponse extends RResponse
{
 
    //attributes
    public $status; 
    
    public $fileIds = [];
}
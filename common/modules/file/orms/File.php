<?php
namespace common\modules\file\orms;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * File model
 *
 */
class File extends ActiveRecord
{
    const BASE_DIR = "storage/";
    
    public static function tableName()
    {
        return '{{%file}}';
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt'
            ],
        ];
    }


}

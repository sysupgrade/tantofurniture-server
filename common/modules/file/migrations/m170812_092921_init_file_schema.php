<?php

use yii\db\Migration;

class m170812_092921_init_file_schema extends Migration
{
    public function safeUp()
    {
        $this->execute("  
    CREATE TABLE file (
      `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `userId` int(11) NOT NULL,
      `path` varchar(255) NOT NULL,
       status smallint(6) not null,
      `createdAt` int(11) NOT NULL,
      `updatedAt` int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

    }

    public function safeDown()
    {
        echo "m170812_092921_init_file_schema cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170812_092921_init_file_schema cannot be reverted.\n";

        return false;
    }
    */
}

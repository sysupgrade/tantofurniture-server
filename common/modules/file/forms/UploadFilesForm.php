<?php
namespace common\modules\file\forms;

use common\modules\user\orms\User;
use common\modules\file\orms\File;
use rkit\components\RService;
use common\modules\file\responses\SuccessUploadFileResponse;
/**
 * UploadFilesForm service
 *
 */
class UploadFilesForm extends RService
{
    
    //attributes
    public $files;
    
    public $authKey;
    
    private $userId;
    
    public function rules() {
        return [
            ['authKey', 'string'],
            ['authKey', 'required'],
            ['authKey', 'getUserId'],
            ['files', 'each', 'rule' => ['file'] ],
            ['files', 'isArrayNotEmpty']
        ];
    }
    
    public function isArrayNotEmpty() {
        if(count($this->files) == 0) {
            $this->addError("files", "No Files Found!");
        }
    }
    
    public function getUserId() {
        $user = User::find()->where(['authKey' => $this->authKey])->one();
        if(!$user) {
            return $this->addError("authKey", "user not found");
        }
        
        $this->userId = $user->id;
    }
    
    public function upload() : ?SuccessUploadFileResponse {
        if(!$this->validate()) {
            return null;
        }
            
        $ids = [];
        
        foreach($this->files as $file) {
            $ids[] = $this->saveFile($file);
        }
        
        $response = new SuccessUploadFileResponse;
        $response->status = count($this->files) ? 1 : 0;
        $response->fileIds = $ids;
        return $response;
    }   
    
    private function saveFile($file) : ?int {
        $dirPath =  File::BASE_DIR . $this->userId;
        $this->createDirectoryIfNotExist($dirPath);
        
        $basename = $file['name'];
        $ext = pathinfo($basename, PATHINFO_EXTENSION);
        $fileName = pathinfo($basename, PATHINFO_FILENAME);
        
        $fileOrm = new File();
        $fileOrm->userId = $this->userId;
        $fileOrm->path = $dirPath . "/" . $basename;
        
        $index = 0;
        while(file_exists($fileOrm->path)) {
            $index++;
            $fileOrm->path = $dirPath . "/" . $fileName . "_" . $index . "." . $ext;
        }
        if(!$fileOrm->save()) {
            return null;
        }
        $success = move_uploaded_file($file['tmp_name'] ,   $fileOrm->path);
        if(!$success) {
            return null;
        }
         
        return intval($fileOrm->id);
    }
    
    private function createDirectoryIfNotExist(string $path) {
        if(!file_exists($path)) {
            mkdir($path, 0755, true);
        }
    }
}
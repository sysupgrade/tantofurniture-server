<?php
namespace common\modules\file\controllers;

use Yii;
use yii\web\Controller;
use common\modules\file\forms\UploadFilesForm;
/**
 * ImageController controller
 */
class ImageController extends Controller
{
    public $enableCsrfValidation = false;
    
      
    public static function allowedDomains() {
        return [
             '*',                        // star allows all domains
//            'http://test1.example.com',
//            'http://test2.example.com',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return array_merge(parent::behaviors(), [

            // For cross-domain AJAX request
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:
                    'Origin'                           => static::allowedDomains(),
                    'Access-Control-Request-Method'    => ['POST'],
                    'Access-Control-Request-Headers' => ['Content-Type'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                ],
            ],

        ]);
    }
    
    public function actionUpload() {
        $form = new UploadFilesForm();
        $form->files = $_FILES;
        $form->authKey = isset($_GET['authKey']) ? $_GET['authKey'] : null;
        $response = $form->upload();
        return $response ? $response->createJson() : json_encode($form->getErrors());

    }
    
}


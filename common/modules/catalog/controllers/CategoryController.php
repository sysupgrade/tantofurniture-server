<?php
namespace common\modules\catalog\controllers;

use Yii;
use common\modules\catalog\forms\ListCategory;
use yii\web\Controller;
use common\modules\catalog\forms\SearchCategory;
use common\modules\catalog\forms\AddCategoryForm;
/**
 * CategoryController controller
 */
class CategoryController extends Controller
{
    public $enableCsrfValidation = false;
    
      
    public static function allowedDomains() {
        return [
             '*',                        // star allows all domains
//            'http://test1.example.com',
//            'http://test2.example.com',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return array_merge(parent::behaviors(), [

            // For cross-domain AJAX request
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:
                    'Origin'                           => static::allowedDomains(),
                    'Access-Control-Request-Method'    => ['POST'],
                    'Access-Control-Request-Headers' => ['Content-Type'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                ],
            ],

        ]);
    }
    
    public function actionAdd() {
        $form = new AddCategoryForm();
        $form->loadData($_POST);
        $response = $form->add();
        return $response ? $response->createJson() : json_encode($form->getErrors());
    }
    
    public function actionList() {
        $form = new ListCategory();
        $form->loadData($_GET);
        $response = $form->list();
        return $response ? $response->createJson() : json_encode($form->getErrors());
    }
    
    public function actionSearch() {
        $form = new SearchCategory();
        $form->loadData($_GET);
        $response = $form->search();
        return $response ? $response->createJson() : json_encode($form->getErrors());
        
    }
    
}


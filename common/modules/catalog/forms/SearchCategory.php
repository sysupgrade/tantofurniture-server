<?php
namespace common\modules\catalog\forms;

use common\modules\shared\responses\SuccessSearchResponse;
use rkit\components\RService;
use common\modules\user\orms\User;
use common\modules\catalog\daos\CategoryDao;
/**
 * SearchCategory service
 *
 */
class SearchCategory extends RService
{
    
    //attributes
    public $authKey;

    public $query;

    private $categoryDao;
    
    public function init() {
        $this->categoryDao = new CategoryDao();
    }
    
    public function rules() {
        return [
//            
//            ['authKey', 'string'],
//            ['authKey', 'required'],
//            ['authKey', 'isUser'],
            
            ['query', 'string']
        ];
    }            
    
    function isUser() {
        $user = User::find()->where(['authKey' => $this->authKey])->one();
        if(!$user) {
            return $this->addError("authKey", "Not a user");
        }
    }
    
    function search() : ?SuccessSearchResponse {
        if(!$this->validate()) {
            return null;
        }
        
        $response = new SuccessSearchResponse();
        $response->status = 1;
        
        $vos = $this->categoryDao->searchCategories($this->query);
        
        $data = [];
        foreach($vos as $vo) {
            $data[$vo->getId()] = $vo->getName();
        }
        $response->data = $data;
        return $response;
    }
}
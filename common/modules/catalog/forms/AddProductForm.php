<?php
namespace common\modules\catalog\forms;

use rkit\components\RService;
/**
 * AddProductForm service
 *
 */
class AddProductForm extends RService
{
    
    //attributes
    public $name;

    public $defaultPrice;

    public $category;

    public $imageIds;

    public function rules() {
        return [
            ['name', 'string'],
            ['name', 'required'],
            
            ['defaultPrice', 'double'],
            ['defaultPrice', 'required'],
            
            ['category', 'string'],
            ['category', 'required'],
            
            ['imageIds', 'each', 'rule' => ['integer']]
        ];
    }            
    
    public function add() {
        if(!$this->validate()) {
            return null;
        }
        
        die(var_dump($this->imageIds));
    }
}
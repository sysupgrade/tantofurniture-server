<?php
namespace common\modules\catalog\forms;

use rkit\components\RService;
use common\modules\user\forms\AuthForm;
use common\modules\catalog\responses\ListCategoryResponse;
use common\modules\user\orms\UserAccess;
use common\modules\catalog\daos\CategoryDao;
/**
 * ListCategory service
 *
 */
class ListCategory extends RService
{
    
    //attributes
    public $authKey;

    private $categoryDao;
    
    public function init() {
        $this->categoryDao = new CategoryDao();
    }
    
    public function rules() {
        return [
            ['authKey', 'string'],  
            ['authKey', 'required'],
            ['authKey', 'authenticate']
        ];
    }  
    
    function authenticate() {
        $authForm = new AuthForm();
        $authForm->accessId = UserAccess::LIST_CATEGORY;
        $authForm->authKey = $this->authKey;
        if(!$authForm->check()) {
            $this->addError("authKey", "Not Allowed");
        }
    }
    
    public function list() : ?ListCategoryResponse {
        if(!$this->validate()) {
            return null;
        }
        
        $response = new ListCategoryResponse();
        $response->status = 1;
        
        $vos = $this->categoryDao->getCategoryList();
        
        $data = [];
        foreach($vos as $vo) {
            $data[] = $vo->getDataArray();
        }
        $response->data = $data;
        return $response;
    }
}
    <?php
namespace common\modules\catalog\forms;

use rkit\components\RService;
use common\modules\catalog\responses\SuccessResponse;
use common\modules\user\orms\UserAccess;
use common\modules\user\forms\AuthForm;
use common\modules\catalog\orms\CatalogCategory;
/**
 * AddCategoryForm service
 *
 */
class AddCategoryForm extends RService
{
    
    //attributes
    public $authKey;

    public $name;

    public $description;

    public function rules() {
        return [
            ['authKey', 'string'],
            ['authKey', 'required'],
            ['authKey', 'authenticate'],
            
            ['name', 'string'],
            ['description', 'string'],
            ['name', 'required']
        ];
    }            
    
    
    function authenticate() {
        $authForm = new AuthForm();
        $authForm->accessId = UserAccess::ADD_NEW_CATEGORY;
        $authForm->authKey = $this->authKey;
        if(!$authForm->check()) {
            $this->addError("authKey", "Not Allowed");
        }
    }
    
    public function add() : ?SuccessResponse {
        if(!$this->validate()) {
            return null;
        }
        
        $category = new CatalogCategory();
        $category->name = $this->name;
        $category->description = $this->description;
        $category->status = CatalogCategory::STATUS_ACTIVE;
        $status = $category->save();
        $response = new SuccessResponse();
        $response->status = $status ? 1 : 0;
        return $response;
        
    }
}
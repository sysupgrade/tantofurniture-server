<?php
namespace common\modules\catalog\responses;

use rkit\components\RResponse;
/**
 * ListCategoryResponse response
 *
 */
class ListCategoryResponse extends RResponse
{
        //attributes
    public $status;

    public $data;
}
<?php
namespace common\modules\catalog\daos;

use Yii;
use rkit\components\Dao;
use common\modules\catalog\vos\CategoryVo;
use common\modules\catalog\orms\CatalogCategory;
/**
 * CategoryDao class
 */
class CategoryDao implements Dao
{
    const CATEGORY_LIST = "select * from catalog_category where status = :status";
    
    const SEARCH_CATEGORIES = "select catalog_category.id, catalog_category.name
                               from catalog_category
                               where catalog_category.name LIKE :query
                                and catalog_category.status = :status";
    
    public function searchCategories(?string $query = "") : ?array {
        $status = CatalogCategory::STATUS_ACTIVE;
        $query = "%" . $query . "%";
        $results = Yii::$app->db
                ->createCommand(self::SEARCH_CATEGORIES)
                ->bindParam(':status', $status)
                ->bindParam(':query', $query)
                ->queryAll();
        
        $vos = [];
        
        foreach($results as $result ) {
            $builder = CategoryVo::createBuilder();
            $builder->loadData($result);
            $vos[] = $builder->build();
        }
        return $vos;
    }
    
    public function getCategoryList() : ?array {
        $status = CatalogCategory::STATUS_ACTIVE;
        $results = Yii::$app->db
                ->createCommand(self::CATEGORY_LIST)
                ->bindParam(':status', $status)
                ->queryAll();
        $vos = [];
        foreach($results as $result ) {
            $builder = CategoryVo::createBuilder();
            $builder->loadData($result);
            $vos[] = $builder->build();
        }
        return $vos;
    }  
}


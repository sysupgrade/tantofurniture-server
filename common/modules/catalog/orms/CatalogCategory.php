<?php
namespace common\modules\catalog\orms;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * CatalogCategory model
 *
 */
class CatalogCategory extends ActiveRecord
{
    const STATUS_ACTIVE = 10;
    
    const STATUS_DELETED = 0;
    
    public static function tableName()
    {
        return '{{%catalog_category}}';
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt'
            ],
        ];
    }


}

<?php
namespace common\modules\catalog\orms;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * CatalogProduct model
 *
 */
class CatalogProduct extends ActiveRecord
{
    const STATUS_ACTIVE = 10;
    
    const STATUS_DELETED = 0;
    
    const STATUS_PENDING = 11;
    
    public static function tableName()
    {
        return '{{%catalog_product}}';
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt'
            ],
        ];
    }


}

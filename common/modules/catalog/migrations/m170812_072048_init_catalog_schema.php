<?php

use yii\db\Migration;

class m170812_072048_init_catalog_schema extends Migration
{
    public function safeUp()
    {
        $this->execute("
    CREATE TABLE catalog_category (
      `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL UNIQUE,
      `description` TEXT NULL,
      status smallint(6) not null,
      `createdAt` int(11) NOT NULL,
      `updatedAt` int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE catalog_product (
      id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
      creatorId int not null,
      name varchar(255) NOT NULL,
      defaultPrice double not null,
      status smallint(6) not null,
      createdAt int(11) NOT NULL,
      updatedAt int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
    CREATE TABLE catalog_product_variant (
      id int(11) not null auto_increment primary key,
      productId int not null,
      name varchar(255) not null,
      widgetType smallint(6) not null,
      status smallint(6) not null,
      createdAt int not null,
      updatedAt int not null,
      foreign key(productId) references catalog_product(id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE catalog_product_variant_type (
      id int(11) not null auto_increment primary key,
      variantId int not null,
      name varchar(255) not null,
      price double not null,
      type smallint(6) not null,
      status smallint(6) not null,
      createdAt int not null,
      updatedAt int not null,
      foreign key(variantId) references catalog_product_variant(id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    CREATE TABLE catalog_product_category (
      productId int not null,
      categoryId int not null,
      status smallint(6) not null,
      createdAt int not null,
      updatedAt int not null,
      foreign key(productId) references catalog_product(id),
      foreign key(categoryId) references catalog_category(id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
    CREATE TABLE catalog_product_image (
      productId int not null,
      imageId int not null,
      status smallint(6) not null,
      createdAt int not null,
      updatedAt int not null,
      foreign key(productId) references catalog_product(id)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

);");
    }

    public function safeDown()
    {
        echo "m170812_072048_init_catalog_schema cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170812_072048_init_catalog_schema cannot be reverted.\n";

        return false;
    }
    */
}

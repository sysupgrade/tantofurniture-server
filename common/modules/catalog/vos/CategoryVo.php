<?php
namespace common\modules\catalog\vos;

use yii\db\ActiveRecord;
use rkit\components\RVo2;
/**
 * CategoryVo vo
 *
 */
class CategoryVo extends RVo2
{
    public static function createBuilder() { return new CategoryVoBuilder();} 
    //attributes

    private $id;

    private $name;

    private $description;

    public function __construct(CategoryVoBuilder $builder) { 
        $this->id = $builder->getId(); 
        $this->name = $builder->getName(); 
        $this->description = $builder->getDescription(); 
    }

    //getters

    public function getId() { 
        return $this->id; 
    }

    public function getName() { 
        return $this->name; 
    }

    public function getDescription() { 
        return $this->description; 
    }
}
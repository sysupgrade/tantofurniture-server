<?php
namespace common\modules\user\responses;

use rkit\components\RResponse;
/**
 * ListFranchiserResponse response
 *
 */
class ListFranchiserResponse extends RResponse
{
 
    //attributes
    public $status;
    
    public $data;
}
<?php
namespace common\modules\user\responses;

use rkit\components\RVo2;
use rkit\components\RResponse;
/**
 * TokenResponse response
 *
 */
class TokenResponse extends RResponse
{
 
    //attributes
    public $status;

    public $authKey;
  

}
<?php
namespace common\modules\user\vos;

use yii\db\ActiveRecord;
use rkit\components\RVoBuilder;
/**
 * UserVo builder
 *
 */
class UserVoBuilder extends RVoBuilder
{
    function build() { return new UserVo($this);  }
    //attributes

    public $id;

    public $firstName;

    public $lastName;

    public $address;

    public $email;

    public function rules() { 
        return [
           ['id','string'],
           ['firstName','string'],
           ['lastName','string'],
           ['address','string'],
           ['email','string'],
        ];
    }

    //getters

    public function getId() { 
        return $this->id; 
    }

    public function getFirstName() { 
        return $this->firstName; 
    }

    public function getLastName() { 
        return $this->lastName; 
    }

    public function getAddress() { 
        return $this->address; 
    }

    public function getEmail() { 
        return $this->email; 
    }

    //setters

    public function setId($id) { 
        $this->id = $id; 
    }

    public function setFirstName($firstName) { 
        $this->firstName = $firstName; 
    }

    public function setLastName($lastName) { 
        $this->lastName = $lastName; 
    }

    public function setAddress($address) { 
        $this->address = $address; 
    }

    public function setEmail($email) { 
        $this->email = $email; 
    }
}
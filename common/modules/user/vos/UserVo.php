<?php
namespace common\modules\user\vos;

use yii\db\ActiveRecord;
use rkit\components\RVo2;
/**
 * UserVo vo
 *
 */
class UserVo extends RVo2
{
    public static function createBuilder() { return new UserVoBuilder();} 
    //attributes

    private $id;

    private $firstName;

    private $lastName;

    private $address;

    private $email;

    public function __construct(UserVoBuilder $builder) { 
        $this->id = $builder->getId(); 
        $this->firstName = $builder->getFirstName(); 
        $this->lastName = $builder->getLastName(); 
        $this->address = $builder->getAddress(); 
        $this->email = $builder->getEmail(); 
    }

    //getters

    public function getId() { 
        return $this->id; 
    }

    public function getFirstName() { 
        return $this->firstName; 
    }

    public function getLastName() { 
        return $this->lastName; 
    }

    public function getAddress() { 
        return $this->address; 
    }

    public function getEmail() { 
        return $this->email; 
    }
}
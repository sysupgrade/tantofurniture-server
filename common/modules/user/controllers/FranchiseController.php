<?php
namespace common\modules\user\controllers;

use Yii;
use yii\web\Controller;
use common\modules\user\forms\AddNewFranchiser;
use common\modules\user\forms\ListFranchiser;
/**
 * FranchiseController controller
 */
class FranchiseController extends Controller
{
    public $enableCsrfValidation = false;
      
    public static function allowedDomains() {
        return [
             '*',                        // star allows all domains
//            'http://test1.example.com',
//            'http://test2.example.com',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return array_merge(parent::behaviors(), [

            // For cross-domain AJAX request
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:
                    'Origin'                           => static::allowedDomains(),
                    'Access-Control-Request-Method'    => ['POST'],
                    'Access-Control-Request-Headers' => ['Content-Type'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                ],
            ],

        ]);
    }
    
    public function actionAdd() {
        $form = new AddNewFranchiser();
        $form->loadData($_POST);
        $response = $form->add();
        return $response ? $response->createJson() : json_encode($form->getErrors());
    }
    
    public function actionList() {
        $form = new ListFranchiser();
        $form->loadData($_GET);
        $response = $form->get();
        return $response ? $response->createJson() : json_encode($form->getErrors());
    }
    
}


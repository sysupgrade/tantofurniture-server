<?php
namespace common\modules\user\controllers;

use Yii;
use common\modules\user\forms\LoginForm;
use yii\web\Controller;
/**
 * Default controller
 */
class DefaultController extends Controller
{
    
    public function init() {
        
    }
    
    public $enableCsrfValidation = false;
    
    public static function allowedDomains() {
        return [
             '*',                        // star allows all domains
//            'http://test1.example.com',
//            'http://test2.example.com',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return array_merge(parent::behaviors(), [

            // For cross-domain AJAX request
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:
                    'Origin'                           => static::allowedDomains(),
                    'Access-Control-Request-Method'    => ['POST'],
                    'Access-Control-Request-Headers' => ['Content-Type'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                ],
            ],

        ]);
    }
    
    public function actionIndex() {
        return 'dsds';
    }
    
    public function actionLogin() {
        $form = new LoginForm();
        $form->loadData($_POST);
        $response = $form->login();
        return $response ? $response->createJson() : json_encode($form->getErrors());
    }
    
}


<?php

use yii\db\Migration;
use common\modules\user\orms\UserAccess;

class m170811_084754_add_list_franchiser extends Migration
{
    public function safeUp()
    {
        $userAccess = new UserAccess();
        $userAccess->id = UserAccess::LIST_FRANCHISER;
        $userAccess->name = "List Franchiser";
        $userAccess->save();
    }

    public function safeDown()
    {
        echo "m170811_084754_add_list_franchiser cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170811_084754_add_list_franchiser cannot be reverted.\n";

        return false;
    }
    */
}

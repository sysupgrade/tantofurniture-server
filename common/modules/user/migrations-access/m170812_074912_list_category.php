<?php

use yii\db\Migration;
use common\modules\user\orms\UserAccess;

class m170812_074912_list_category extends Migration
{
    public function safeUp()
    {
        $userAccess = new UserAccess();
        $userAccess->id = UserAccess::LIST_CATEGORY;
        $userAccess->name = "List Category";
        $userAccess->save();
    }

    public function safeDown()
    {
        echo "m170812_074912_list_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170812_074912_list_category cannot be reverted.\n";

        return false;
    }
    */
}

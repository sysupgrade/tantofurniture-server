<?php

use yii\db\Migration;
use common\modules\user\orms\UserAccess;

class m170810_141839_add_new_franchiser extends Migration
{
    public function safeUp()
    {
        $userAccess = new UserAccess();
        $userAccess->id = UserAccess::ADD_NEW_FRANCHISER;
        $userAccess->name = "Add New Franchiser";
        $userAccess->save();
    }

    public function safeDown()
    {
        echo "m170810_141839_add_new_franchiser cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170810_141839_add_new_franchiser cannot be reverted.\n";

        return false;
    }
    */
}

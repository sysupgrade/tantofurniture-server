<?php

use yii\db\Migration;
use common\modules\user\orms\UserAccess;

class m170812_074906_add_category extends Migration
{
    public function safeUp()
    {
        $userAccess = new UserAccess();
        $userAccess->id = UserAccess::ADD_NEW_CATEGORY;
        $userAccess->name = "Add New Category";
        $userAccess->save();
    }

    public function safeDown()
    {
        echo "m170812_074906_add_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170812_074906_add_category cannot be reverted.\n";

        return false;
    }
    */
}

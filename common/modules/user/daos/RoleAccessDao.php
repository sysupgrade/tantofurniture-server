<?php
namespace common\modules\user\daos;

use Yii;
use rkit\components\Dao;
/**
 * RoleAccessDao class
 */
class RoleAccessDao implements Dao
{
    const CHECK_ACCESS_ROLE = "SELECT user.id
                            FROM user, user_user_role, user_role_access
                            where (user.authKey = :authKey and user.id = user_user_role.userId AND	
                            user_role_access.accessId = :accessId and user_user_role.roleId = user_role_access.roleId)
                            UNION
                            SELECT user.id
                            from user, user_user_role
                            where user.authKey = :authKey and user_user_role.userId = user.id and user_user_role.roleId = :adminId;
   ";
    
    public function checkAccessRole($authKey, $accessId) : bool {
        $adminId = \common\modules\user\orms\UserRole::ADMIN_ID;
        $result = Yii::$app->db
                ->createCommand(self::CHECK_ACCESS_ROLE)
                ->bindParam(':authKey', $authKey)
                ->bindParam(':accessId', $accessId)
                ->bindParam(':adminId', $adminId)
                ->queryOne();
        return count($result) > 0;
        
    }  
    
}


<?php
namespace common\modules\user\daos;

use Yii;
use common\modules\user\orms\UserRole;
use common\modules\user\orms\UserAccess;
use common\modules\user\orms\User;
use rkit\components\Dao;
use common\modules\user\vos\UserVo;
/**
 * UserDao class
 */
class UserDao implements Dao
{
    
    const GET_FRANCHISER_LIST = "select user.*, user_email_authentication.email
            from user, user_user_role, user_email_authentication
            where user.id = user_user_role.userId and user_user_role.roleId = :franchiserId
                and user.id = user_email_authentication.userId
            and user.status = :activeStatus";
    
    /**
     * 
     * @return UserVo[]
     */
    public function getFranchiserList() : array {
        $franchiserId = UserRole::FRANCHISER_ID;
        $active = User::STATUS_ACTIVE;
        $results = Yii::$app->db
                ->createCommand(self::GET_FRANCHISER_LIST)
                ->bindParam(':franchiserId', $franchiserId)
                ->bindParam(':activeStatus', $active)
                ->queryAll();
        $vos = [];
                
        foreach($results as $result ) {
            $builder = UserVo::createBuilder();
            $builder->loadData($result);
            $vos[] = $builder->build();
        }
        return $vos;
    }
}


<?php
namespace common\modules\user\orms;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
/**
 * Tag model
 *
 */
class UserRole extends ActiveRecord
{
    const ADMIN_ID = 1;
    
    const FRANCHISER_ID = 2;
    
    public static function tableName()
    {
        return '{{%user_role}}';
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt'
            ],
        ];
    }


}

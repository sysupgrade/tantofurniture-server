<?php
namespace common\modules\user\orms;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Tag model
 *
 */
class UserAccess extends ActiveRecord
{
    const ADD_NEW_FRANCHISER = 1;
    
    const LIST_FRANCHISER = 2;
    
    const ADD_NEW_CATEGORY = 3;
    
    const LIST_CATEGORY = 4;
    public static function tableName()
    {
        return '{{%user_access}}';
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt'
            ],
        ];
    }


}

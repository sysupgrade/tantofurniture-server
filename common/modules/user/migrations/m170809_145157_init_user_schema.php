<?php

use yii\db\Migration;

class m170809_145157_init_user_schema extends Migration
{
    public function safeUp()
    {
$this->execute("
CREATE TABLE user (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL UNIQUE,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `createdAt` int(11) NOT NULL,
  `updatedAt` int(11) NOT NULL,
  `authKey` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  telephone varchar(255) NULL,
  address text NULL,
  lat FLOAT( 10, 6 ) NULL ,
  lng FLOAT( 10, 6 ) NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE user_email_authentication (
  userId int(11) NOT NULL PRIMARY KEY,
  passwordHash varchar(255) NOT NULL,
  passwordResetToken varchar(255) NULL,
  isPrimaryEmail tinyint(1) NOT NULL DEFAULT '1',
  email varchar(100) NOT NULL UNIQUE,
  isValidated tinyint(1) NOT NULL DEFAULT '0',
  createdAt int(11) NOT NULL,
  updatedAt int(11) NOT NULL,
  FOREIGN KEY (userId) references user(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE user_role(
  id int(11) not null primary key,
  name varchar(255) not null,
  description text null,
  createdAt int not null,
  updatedAt int not null
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE user_access (
  id int(11) not null primary key,
  name varchar(255) not null,
  description text null,
  createdAt int not null,
  updatedAt int not null
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE user_role_access(
  roleId int not null,
  accessId int not null,
  createdAt int not null,
  updatedAt int not null,
  foreign key(roleId) references user_role(id),
  foreign key(accessId) references user_access(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE user_user_role(
  userId int not null,
  roleId int not null,
  createdAt int not null,
  updatedAt int not null,
  foreign key(userId) references user(id),
  foreign key(roleId) references user_role(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
");
    }

    public function safeDown()
    {
        echo "m170809_145157_init_user_schema cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170809_145157_init_user_schema cannot be reverted.\n";

        return false;
    }
    */
}

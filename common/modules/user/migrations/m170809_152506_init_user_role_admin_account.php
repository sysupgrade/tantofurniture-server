<?php

use yii\db\Migration;
use common\modules\user\orms\User;
use common\modules\user\orms\UserRole;
use common\modules\user\orms\UserUserRole;
use common\modules\user\forms\SignupForm;

class m170809_152506_init_user_role_admin_account extends Migration
{
    public function safeUp()
    {
        $role = new UserRole();
        $role->id = UserRole::ADMIN_ID;
        $role->name = "Admin";
        $role->save();
        
        $signup = new SignupForm();
        $signup->firstName = "Admin";
        $signup->lastName = "Admin";
        $signup->email = "admin@admin.com";
        $signup->password = "password";
        $responseToken = $signup->signup();
        
        $userId = $this->getUserId($responseToken->authKey);
        
        $userRole = new UserUserRole();
        $userRole->userId = $userId;
        $userRole->roleId = $role->id;
        $userRole->save();
        
        $role = new UserRole();
        $role->id = UserRole::FRANCHISER_ID;
        $role->name = "Franchiser";
        $role->save();
    }
    
    private function getUserId(string $authKey) {
        return User::find()->where(['authKey' => $authKey])->one()['id'];
    }
    
    public function safeDown()
    {
        echo "m170809_152506_init_user_role_admin_account cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170809_152506_init_user_role_admin_account cannot be reverted.\n";

        return false;
    }
    */
}

<?php
namespace common\modules\user\forms;

use rkit\components\RService;
use common\modules\user\orms\User;
use common\modules\user\orms\UserEmailAuthentication;
use common\modules\user\responses\TokenResponse;
/**
 * SignupForm service
 *
 */
class SignupForm extends RService
{
    
    //attributes
    public $firstName;

    public $lastName;

    public $password;

    public $email;


    public function rules() {
        return [
            
            ['firstName', 'string'],
            ['lastName', 'string'],
            ['firstName', 'required'],
            
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\modules\user\orms\UserEmailAuthentication', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }
    
    public function signup() : ?TokenResponse {
        if(!$this->validate()) {
            return null;
        }        
        $user = $this->createUser();
        if(!$user) {
            return null;
        }
        if(!$this->createUserEmailAuth($user->id)) {
            return null;
        }
        return $this->buildResponse($user);
    }
    
    private function buildResponse(User $user) : TokenResponse {
        $response = new TokenResponse();
        $response->status = 1;
        $response->authKey = $user->authKey;
        return $response;
    }
    
    /**
     * 
     * @return int userId
     */
    private function createUser() : ?User {
        $user = new User();
        $user->firstName = $this->firstName;
        $user->lastName = $this->lastName;
        $user->status = User::STATUS_ACTIVE;
        $user->generateAuthKey();
        $user->generateUsername();
        return $user->save() ? $user : null;
        
    }
    
    private function createUserEmailAuth(int $userId) : bool {
        $userEmailAuth = new UserEmailAuthentication();
        $userEmailAuth->email = $this->email;
        $userEmailAuth->userId = $userId;
        $userEmailAuth->setPassword($this->password);
        return $userEmailAuth->save();
    }
    
}
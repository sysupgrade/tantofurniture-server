<?php
namespace common\modules\user\forms;

use common\modules\user\responses\ListFranchiserResponse;
use common\modules\user\orms\UserAccess;
use common\modules\user\forms\AuthForm;
use common\modules\user\daos\UserDao;
use rkit\components\RService;
/**
 * ListFranchise service
 *
 */
class ListFranchiser extends RService
{
    
    //attributes
    public $authKey;
    
    private $userDao;
    
    public function init() {
        $this->userDao = new UserDao();
    }
    
    public function rules() {
        return [
            ['authKey', 'string'],
            ['authKey', 'required'],
            ['authKey', 'authenticate']
        ];
    }            
    
    function authenticate() {
        $authForm = new AuthForm();
        $authForm->accessId = UserAccess::ADD_NEW_FRANCHISER;
        $authForm->authKey = $this->authKey;
        if(!$authForm->check()) {
            $this->addError("authKey", "Not Allowed");
        }
    }
    
    public function get() : ?ListFranchiserResponse {
        if(!$this->validate()) {
            return null;
        } 
        return $this->createResponse();
    }
    
    private function createResponse() : ListFranchiserResponse {
        $response = new ListFranchiserResponse();
        $response->status = 1;
        
        $vos = $this->userDao->getFranchiserList();
        
        $data = [];
        foreach($vos as $vo) {
            $data[] = $vo->getDataArray();
        }
        $response->data = $data;
        return $response;
    }

}
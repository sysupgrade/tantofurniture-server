<?php
namespace common\modules\user\forms;

use rkit\components\RService;
use common\modules\user\daos\RoleAccessDao;
/**
 * AuthForm service
 *
 */
class AuthForm extends RService
{
    
    //attributes
    public $authKey;

    public $accessId;
    
    private $roleAccessDao;
    
    public function init() {
        $this->roleAccessDao = new RoleAccessDao();
    }

    public function rules() {
        return [
            ['authKey', 'string'],
            ['authKey', 'required'],
            ['authKey', 'authenticate'],
            
            ['accessId', 'integer'],
            ['accessId', 'required']
        
        ];
    }      
    
    function authenticate() : void {
        if(!$this->roleAccessDao->checkAccessRole($this->authKey, $this->accessId)) {
            $this->addError("authKey", "Error!");
        }
    }
    
    public function check() : bool {
        return $this->validate();
    }
}
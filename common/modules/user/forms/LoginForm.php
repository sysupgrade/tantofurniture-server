<?php
namespace common\modules\user\forms;

use rkit\components\RService;
use common\modules\user\orms\User;
use common\modules\user\responses\TokenResponse;
use common\modules\user\orms\UserEmailAuthentication;
/**
 * LoginForm service
 *
 */
class LoginForm extends RService
{
    
    //attributes
    public $email;

    public $password;
    
    private $userEmail;

    public function rules() {
        return [

            // username and password are both required
            [['email', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }            
    
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->userEmail = $this->getUserEmail();
            if (!$this->userEmail || !$this->userEmail->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }
    
    protected function getUserEmail() {
        return UserEmailAuthentication::find()->where(['email' => $this->email])->one();
    }


    public function login() : ?TokenResponse {
        if(!$this->validate()) {
            return null;
        }
        
        $response = new TokenResponse();
        $response->status = 1;
        $response->authKey = User::find()->where(['id' => $this->userEmail->userId])->one()['authKey'];
        return $response;
    }
}
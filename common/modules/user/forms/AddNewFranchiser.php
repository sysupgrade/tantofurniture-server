<?php
namespace common\modules\user\forms;

use rkit\components\RService;
use common\modules\user\responses\SuccessResponse;
use common\modules\user\orms\User;
use common\modules\user\orms\UserUserRole;
use common\modules\user\orms\UserEmailAuthentication;
/**
 * AddNewFranchiser service
 *
 */
class AddNewFranchiser extends RService
{
    
    //attributes
    public $firstName;

    public $lastName;

    public $email;

    public $password;
    
    public $authKey;

    public function rules() {
        return [
            ['authKey', 'string'],
            ['authKey', 'required'],
            ['authKey', 'authenticate'],
            
            ['firstName', 'string'],
            ['lastName', 'string'],
            ['firstName', 'required'],
            
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\modules\user\orms\UserEmailAuthentication', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6]
        ];
    }            
    
    function authenticate() {
        $authForm = new AuthForm();
        $authForm->accessId = \common\modules\user\orms\UserAccess::ADD_NEW_FRANCHISER;
        $authForm->authKey = $this->authKey;
        if(!$authForm->check()) {
            $this->addError("authKey", "Not Allowed");
        }
    }
    
    
    public function add() : ?SuccessResponse {
        if(!$this->validate()) {
            return null;
        }        
        $user = $this->createUser();
        if(!$user) {
            return null;
        }
        if(!$this->createUserEmailAuth($user->id)) {
            return null;
        }
        
        if(!$this->addToRole($user->id)) {
            return null;
        }
        
        return $this->buildResponse($user);
    }
    
    private function buildResponse(User $user) : SuccessResponse {
        $response = new SuccessResponse();
        $response->status = 1;
        return $response;
    }
    
    /**
     * 
     * @return int userId
     */
    private function createUser() : ?User {
        $user = new User();
        $user->firstName = $this->firstName;
        $user->lastName = $this->lastName;
        $user->status = User::STATUS_ACTIVE;
        $user->generateAuthKey();
        $user->generateUsername();
        return $user->save() ? $user : null;
        
    }
    
    private function addToRole(int $userId) : bool {
        $userUserRole = new UserUserRole();
        $userUserRole->userId = $userId;
        $userUserRole->roleId = \common\modules\user\orms\UserRole::FRANCHISER_ID;
        return $userUserRole->save() ? 1 : 0 ;
    }
    
    private function createUserEmailAuth(int $userId) : bool {
        $userEmailAuth = new UserEmailAuthentication();
        $userEmailAuth->email = $this->email;
        $userEmailAuth->userId = $userId;
        $userEmailAuth->setPassword($this->password);
        return $userEmailAuth->save();
    }
    
    
}
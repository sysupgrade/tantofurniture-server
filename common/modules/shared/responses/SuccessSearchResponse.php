<?php
namespace common\modules\shared\responses;

use rkit\components\RResponse;
/**
 * SuccessSearchResponse response
 *
 */
class SuccessSearchResponse extends RResponse
{
 
    //attributes
    public $status;
    
    public $data;


       
}
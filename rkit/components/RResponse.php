<?php
namespace rkit\components;

abstract class RResponse {
    
    public function createJson() : string {
        return json_encode(get_object_vars($this));
    }
}